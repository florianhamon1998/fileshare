<!DOCTYPE html>
<html lang=fr>
    <head>
        <meta charset="utf-8">
        <title> FileShare</title>
        <link rel="stylesheet" href="style.css">
    </head>

    <header>

      <div id="fondmenu">
        <nav class="menu-nav">
      <ul id="menu">
        <h1 id=titre>FileShare</h1>
          <li class="btn">
            <a href="../Share/share.php">
              Partage tes photos
            </a>
          </li>

          <li class="btn">
            <a href="../Contact/contact.html">
            Nous contacter
            </a>
          </li>

          <li class="btn">
            <a href="../Connexion/Connexion.php">
              Se connecter
            </a>
          </li>

      </ul>
    </nav>
    <p id=slogan>Partage tes fichiers plus vite que la lumière </p> <br>
  </header>

    <body>

            <center>
            <h4>Connectez-vous afin de pouvoir partager <br> et
                recevoir des fichiers avec les autres membres</h4>
            <form method="post">

              <p>Adresse e-mail</p>
              <input type="email" name="email" size="30"id="email"placeholder="Votre adresse e-mail"required>
            <br>
            <p>Mot de passe</p>
            <input type="password" name="password"id="password" minlength="8" size="30"placeholder="Votre mot de passe"required>
<br>

                <input type="submit" name="formlogin" id="formlogin" value="Se connecter">
            <br>
            <a href="../register/register.php">
                Je n'ai pas de compte
            </a>
            </form>
            <?php
            include'../database.php';
            global $db;
                if(isset($_POST['formlogin']))
                {
                  $email=$_POST["email"];
                  $password=$_POST["password"];
                  if(!empty($email) && !empty($password))
                  {
                    $q = $db->prepare ("SELECT * FROM users WHERE email =:email");
                    $q->execute(['email' => $email]);
                    $result = $q->fetch();

                    if ($result == true)
                    {
                      //le compte existe
                      $hashpassword = $result['password'];
                      if(password_verify($password, $hashpassword))
                      {
                        SESSION_START();
                        $_SESSION["pseudo"]=$result['pseudo'];
                        $_SESSION["email"]=$email;
                        header("Location: ../Share/share.php");
                      }
                      else {
                        echo "Le mot de passe est incorect";
                      }
                    }
                    else {
                      {
                        echo"Le compte portant comme e-mail " .$email." n'existe pas";
                      }
                    }
                  }
                  else
                  {
                    echo "L'email ou le mot de passe est incorect";
                  }
                }
            ?>
            </center>

        </div>
    </body>
    <!--bas de la page-->
    <footer>
    <p>
      Copyright &copy; FileShare - 2020-2020 - All Right Reserved
    </P>
    </footer>
</html>
